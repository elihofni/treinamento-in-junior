# Projeto Base Treinamento IN Junior

Treinamento IN Junior

---

## Pré-requisitos
[npm + Node.js](https://www.npmjs.com/get-npm)

## Trabalhando com essa estrutura

`npm install` instala dependências

`npm start` inicia o projeto localmente na porta 3000 (`http://localhost:3000/`)

`npm run lint` para reportar manualmente erros de padrão de código no JS e SCSS

`npm run build`  para gerar os arquivos de produção

`npm run share` Usa [ngrok](https://github.com/bubenshchykov/ngrok). Com o projeto rodando na porta 3000, execute o comando `npm run share` em outra aba no terminal. Um link para compartilhar o projeto será gerado.

---

## Padrão de código
### Ferramentas
* [Husky](https://github.com/typicode/husky) cria githook e previne commits ruins
* [editorconfig](http://editorconfig.org/) é uma ferramenta para manter o estilo do código consistente entre diferentes editores e IDEs
* [stylelint](https://github.com/stylelint/stylelint) é um linter de SCSS
* [ESLint](http://eslint.org/) é um linter de JS

### Configurando as ferramentas dentro do editor de código
#### Editorconfig
* Sublime => Instala [um plugin](https://github.com/sindresorhus/editorconfig-sublime) para o editorconfig
* Webstorm => Instala, o plugin do editorconfig. Ir para `Preferences > Plugins` e buscar por editorconfig

#### ESlint
* Sublime => Instala [SublimeLinter](http://www.sublimelinter.com/en/latest/installation.html) e [SublimeLinter-eslint](https://github.com/roadhump/SublimeLinter-eslint)
* Webstorm => Habilite o ESLint em `Preferences > Language & Frameworks > Javascript > Code Quality Tools > ESLint`

#### Stylelint
* Primeiro intale [stylelint](https://github.com/stylelint/stylelint) globalmente `npm i stylelint -g`
* Sublime => Instalar [SublimeLinter](http://www.sublimelinter.com/en/latest/installation.html) e [SublimeLinter-contrib-stylelint](https://github.com/kungfusheep/SublimeLinter-contrib-stylelint)
* Webstorm => O Stylelint pode ser habilitado em `Preferences > Languages & Frameworks > Stylesheets > Stylelint`

### Regras para padronização de código
* [Airbnb JS Style Guide](https://github.com/airbnb/javascript) é um padrão de configuração do ESLint criado pela AirBnb
* [stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard) Configuração padrão para o stylelint
* [JSDoc](http://usejsdoc.org/about-getting-started.html) é exigido em todas as funções

---

## Exibindo dados nos templates do pug

Para expor os dados de um JSON dentro dos arquivos .pug, é preciso definir esses dados do JSON dentro do plugin HtmlWebpackPlugin, que fica configurado nos arquivos `webpack.config.dev.js` e `webpack.config.prod.js`

Exemplo:

```
const data = require('./src/data/data.json');
plugins: [
    new HtmlWebpackPlugin({
      data,
      title: 'Index',
      favicon: './favicon.png',
      filename: 'index.html',
      template: './src/index.pug',
    })
]
```

Dentro do pug os dados estarão disponíveis em:

```
p= data
```

---

## Outras ferramentas
* [webpack 3](https://webpack.js.org/) como um empacotador de código para juntar os módulos de ES6 e gerar um pacote final(index.html, main.css e bundle.js)
* [babel](http://babeljs.io/) para transpilar código ES6+ para JavaScript compatível com os browsers mais antigos
